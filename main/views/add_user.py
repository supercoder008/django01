# Create your views here.
from django.views.generic import CreateView

from main.model.user import User


class UserCreateView(CreateView):
    model = User
    fields = ('username', 'first_name', 'last_name', 'email')
