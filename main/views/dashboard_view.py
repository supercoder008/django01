from django.shortcuts import render
from main.model.projects import Project

def dashboard_view(request):
    context = {}
    context['projects'] = list(Project.objects.all())

    return render(request, 'main' / 'dashboard.html')