import json

from django.core.exceptions import FieldError
from django.http import HttpResponseBadRequest, JsonResponse, HttpResponseNotFound
from django.views import View

from main.model.user import User


class UserView(View):
    model = User

    def get(self, request, user_id=''):
        email_filter = request.GET.get('email', '')
        sort_by_field = request.GET.get('sort_by', 'last_name')
        if user_id == '':
            try:
                all_users = self.model.objects.order_by(sort_by_field)
            except FieldError:
                return HttpResponseBadRequest(json.dumps(
                    {'error': 'incorrect value for sort_by parameter'}
                ), content_type='application/json')

            if email_filter:
                all_users = all_users.filter(email__contains=email_filter)
            return JsonResponse(list(all_users.values('username', 'email', 'first_name', 'last_name', 'role')), safe=False)
        else:
            user = list(self.model.objects
                        .filter(id=user_id)
                        .values('username', 'email', 'first_name', 'last_name', 'role'))
            return JsonResponse(user, safe=False) if len(user) > 0 else HttpResponseNotFound()
