from django.http import JsonResponse, HttpRequest

from main.model.usergroups import UserGroup


def list_user_groups(request):
    all_groups = list(UserGroup.objects.all().values())

    return JsonResponse(all_groups, safe=False)
