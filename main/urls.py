from django.urls import path, include

from main.views.add_user import UserCreateView
from main.views.user_view import UserView
from main.views.dashboard_view import dashboard_view

urlpatterns = [
    path('user/', include([
        path('', UserView.as_view()),
        path('<int:user_id>', UserView.as_view())
    ])),
    path('user/add', UserCreateView.as_view(), name='create user'),
    path('dashboard', dashboard_view())
]
