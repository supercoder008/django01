from main.model.usergroups import UserGroup
from django.contrib import admin

from .forms import CustomUserForm
from .model.user import User


class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'email', 'first_name', 'last_name', 'role']

    form = CustomUserForm
    model = User

# Register your models here.
admin.site.register(User, UserAdmin)
admin.site.register(UserGroup)
