from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from main.model.user import User
from main.model.usergroups import UserGroup


class CustomUserForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='First name')
    last_name = forms.CharField(max_length=30, required=True, help_text='Last name')
    email = forms.EmailField(max_length=254, help_text='Email')
    role = forms.TypedChoiceField(
        choices=map(lambda group: (group.id, group.group_name), list(UserGroup.objects.all())),
        coerce=lambda id: UserGroup.objects.get(id=id),
        empty_value=UserGroup.objects.get(group_name='viewer'))
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')