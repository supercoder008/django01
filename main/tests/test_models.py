
from django.test import TestCase
from main.model.user import User
from main.model.usergroups import UserGroup

class UserModelTest(TestCase):
    model = User

    def setUp(self):
        self.model.objects.create(first_name='John',last_name='Doe', username='user01', email='user01@example.com')
        self.model.objects.create(first_name='Jane',last_name='Johns', username='user02', email='user02@example.com')

        UserGroup(group_name='viewer').save()

    def test_user_count(self):
        self.assertEqual(len(list(self.model.objects.all())), 2)

    def test_correct_default_group(self):
        users = self.model.objects.all()
        viewer_group_id = UserGroup.objects.get(group_name='viewer').pk
        for user in users:
            self.assertEqual(user.role.id, viewer_group_id)

    def test_delete(self):
        user = self.model.objects.get(username='user01')
        user.delete()
        self.assertEqual(len(list(self.model.objects.all())), 1)
        user.save()


class UserGroupModelTest(TestCase):
    model = UserGroup

    def setUp(self):
        self.model.objects.create(group_name='viewer')
        self.model.objects.create(group_name='editor')
        self.model.objects.create(group_name='admin')

    def test_user_count(self):
        self.assertEqual(len(list(self.model.objects.all())), 3)

    def test_delete(self):
        group = self.model.objects.get(group_name='editor')
        group.delete()
        self.assertEqual(len(list(self.model.objects.all())), 2)
        group.save()