from django.contrib.auth import validators
from django.db import models
from main.model.user import User


class NetworkScheme(models.Model):
    name = models.CharField(max_length=50)
    version = models.IntegerField()
    creation_date = models.DateField()
    creator = models.ForeignKey(User)
    update_date = models.DateField()
    updater = models.ForeignKey(User)

    class Meta:
        app_label = 'main'
