from django.contrib.auth import validators
from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from main.model.usergroups import UserGroup


class User(AbstractBaseUser):
    USERNAME_FIELD = 'username'

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    username = models.CharField(max_length=50, unique=True, validators=[validators.UnicodeUsernameValidator()])
    email = models.EmailField()
    password = models.CharField(max_length=128)
    role = models.ForeignKey(UserGroup, on_delete=models.SET_NULL, null=True, default=1)

    class Meta:
        app_label = 'main'
        db_table = 'users'
