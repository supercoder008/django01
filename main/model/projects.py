from django.db import models
from main.model.network_schemes import NetworkScheme


class Project(models.Model):
    name = models.CharField(max_length=50)
    network_schemes = models.ManyToManyField(NetworkScheme)

    class Meta:
        app_label = 'main'