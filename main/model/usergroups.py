from django.db import models


class UserGroup(models.Model):
    group_name = models.CharField(max_length=50)

    def __str__(self) -> str:
        return self.group_name
